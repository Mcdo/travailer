#include "listepalindromes.h"
CNoeudPalindrome::CNoeudPalindrome(CPalindrome * unPalindrome)
	:m_pLeSuivant(0)
{
	m_pLeElement = unPalindrome;
}

CNoeudPalindrome::CNoeudPalindrome(const CNoeudPalindrome & unNoeud)
{
	*this = unNoeud;
}

CListePalindromes::CListePalindromes(void(*pfncCallback)(CPalindrome *pUnPalindrome))
	: m_pfncCallback(pfncCallback)
{
}
CListePalindromes::CListePalindromes(const CListePalindromes & pLesPalindromes)
{
	*this = pLesPalindromes;
}
CListePalindromes::~CListePalindromes()
{
	Vider();
}

CListePalindromes & CListePalindromes::operator=(const CListePalindromes & pLesPalindromes)
{
	Vider();
	for (int i = 0; i < pLesPalindromes.NbElements(); i++)
	{
		Inserer(& pLesPalindromes[i]);
	}
	return *this;
}

bool CListePalindromes::Retirer(unsigned int uiIndice)
{
	CNoeudPalindrome * noeudRetirer = m_pLePremier;
	unsigned int u_iCompteur = 0;
	bool bRetour = false;

	if (m_pLePremier != 0)
	{
		if (uiIndice == 0)
		{
			m_pLePremier = m_pLePremier->m_pLeSuivant;

			m_pfncCallback(noeudRetirer->m_pLeElement);
			noeudRetirer->m_pLeSuivant = 0;
			delete noeudRetirer;
			bRetour = true;

	    }
		else
		{
			u_iCompteur = 0;
			while (noeudRetirer != 0 && u_iCompteur < uiIndice)
			{
				noeudRetirer = noeudRetirer->m_pLeSuivant;
				u_iCompteur += 1;
			}
			if (u_iCompteur < uiIndice)
			{
				bRetour = false;
			}
			else
			{
				CNoeudPalindrome * noeudPrecedant = m_pLePremier;

				while (noeudPrecedant->m_pLeSuivant != noeudRetirer)
				{
					noeudPrecedant = noeudPrecedant->m_pLeSuivant;
				}
				noeudPrecedant->m_pLeSuivant = noeudRetirer->m_pLeSuivant;


				m_pfncCallback(noeudRetirer->m_pLeElement);
				noeudRetirer->m_pLeSuivant = 0;
				delete noeudRetirer;
				bRetour = true;
			}
		}
	}


	return bRetour;
}

bool CListePalindromes::EstVide() const
{
	return ( m_pLePremier == 0);
}

int CListePalindromes::NbElements() const
{
	int i = 0;
	CNoeudPalindrome * noeud = m_pLePremier;

	while (noeud != 0)
	{
		noeud = noeud->m_pLeSuivant;
		i += 1;
	}
	return i;
}


void CListePalindromes::Vider() const
{
	CNoeudPalindrome * noeudSupprime= m_pLePremier;
	CNoeudPalindrome * noeudSuivant;
	do
	{
		noeudSuivant = noeudSupprime->m_pLeSuivant;
		m_pfncCallback(noeudSupprime->m_pLeElement);
		noeudSupprime->m_pLeSuivant = 0;
		delete noeudSupprime;
		noeudSupprime= noeudSuivant;
	} while (noeudSupprime != 0);
}

bool CListePalindromes::Inserer(CPalindrome * pUnPalindrome)
{
	CNoeudPalindrome * noeud = 0;
	noeud = new CNoeudPalindrome(pUnPalindrome);
	bool bRetour = false;

	if (m_pLePremier == 0)
	{
		m_pLePremier = noeud;
		bRetour = true;
	}
	else
	{
		CNoeudPalindrome * noeudComparaison = m_pLePremier;
		while (noeudComparaison->m_pLeSuivant != 0 &&
			 *noeudComparaison->m_pLeSuivant->m_pLeElement < *noeud->m_pLeElement  )
		{
			noeudComparaison = noeudComparaison->m_pLeSuivant;
		}
		if (noeudComparaison->m_pLeSuivant == 0)
		{
			noeud->m_pLeSuivant = 0;
			noeudComparaison->m_pLeSuivant = noeud;
			bRetour = true;
		}
		else if (*noeud->m_pLeElement != *noeudComparaison->m_pLeSuivant->m_pLeElement)
		{
			noeud->m_pLeSuivant = noeudComparaison->m_pLeSuivant;
			noeudComparaison->m_pLeSuivant = noeud;
			bRetour = true;
		}

	}
	if (!bRetour)
	{
		m_pfncCallback(pUnPalindrome);
	}
	return bRetour;
}

CPalindrome & CListePalindromes::operator[](unsigned int uiIndice) const
{
	if (m_pLePremier == 0)
	{
		throw ("La liste est vide");
	}
	else if (uiIndice < 0)
	{
		throw("Indice en dehors de la plage.");
	}
	CNoeudPalindrome * noeud = m_pLePremier;
	unsigned int i = 0;
	while (noeud->m_pLeSuivant != 0 && i < uiIndice)
	{
		i += 1;
		if (i < uiIndice)
		{
			noeud = noeud->m_pLeSuivant;
		}
	}
	if (uiIndice == i)
		return *(noeud->m_pLeElement);
	else
		throw("Indice en dehors de la plage.");
}
