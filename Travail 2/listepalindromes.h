/******************************************************************************
	Fichier:	listepalindromes.cpp

	Classe:		listepalindromes

	Auteur:		Mikhail Kamara et Matthieu Martineau

	Utilite:	Declaration de la classe listepalindromes............
******************************************************************************/
#ifndef LISTEPALINDROMES_H
#define LISTEPALINDROMES_H

#include "palindrome.h"
using namespace std;

class CNoeudPalindrome
{
	friend class CListePalindromes;

private:
	CNoeudPalindrome(CPalindrome * unPalindrome =0 );
	CNoeudPalindrome(const CNoeudPalindrome & unNoeud);
	
	CPalindrome  *m_pLeElement;
	CNoeudPalindrome * m_pLeSuivant;
};

class CListePalindromes
{
public:
	CListePalindromes(void(* pfncCallback)(CPalindrome * pUnPalindrome) =0);
	CListePalindromes(const CListePalindromes & unPalindrome);
	~CListePalindromes();
	CListePalindromes & operator = (const CListePalindromes & m_pLesPalindromes);
	bool Retirer(unsigned int uiIndice);
	bool EstVide() const;
	int NbElements() const;
	void Vider() const;
	bool Inserer(CPalindrome * pUnPalindrome);
	CPalindrome & operator[] (unsigned int uiIndice) const;
	

private:
	CNoeudPalindrome * m_pLePremier;
	void(*m_pfncCallback)(CPalindrome * pUnPalindrome);


};
#endif