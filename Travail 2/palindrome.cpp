/******************************************************************************
	Fichier:	palindrome.cpp

	Classe:		CPalindrome

	Auteur:		Mikhail Kamara et Matthieu Martineau

	Utilit�:	Impl�mentation des m�thodes de la classe CPalindrome.
******************************************************************************/

#include "palindrome.h"
/******************************************************************************
	Ce constructeur permet de cr�er un palindrome qui contient 
	entre 3 et 6 caract�re dans l'attribut m_szPalindrome.
******************************************************************************/

CPalindrome::CPalindrome()
{
	m_iNbCaracteres = GenererNbAleatoire(3, 6);
	m_szPalindrome = new char [m_iNbCaracteres + 1];
	int voyelleOuConsonne = GenererNbAleatoire(1, 2);
	int nombreAleatoire;

	for (int i = 0; i < m_iNbCaracteres/2; i++)
	{
		if (voyelleOuConsonne % 2 == 0)
		{
			nombreAleatoire = GenererNbAleatoire(0, 20);
			m_szPalindrome[i] = kszConsonnes[nombreAleatoire];
			m_szPalindrome[m_iNbCaracteres-i-1] = kszConsonnes[nombreAleatoire];
				voyelleOuConsonne += 1;
		}
		else
		{
			nombreAleatoire = GenererNbAleatoire(0, 5);
			m_szPalindrome[i] = kszVoyelles[nombreAleatoire];
			m_szPalindrome[m_iNbCaracteres-i-1] = kszVoyelles[nombreAleatoire];
				voyelleOuConsonne += 1;
		}	
	}
	if (m_iNbCaracteres % 2 == 1)
	{
		nombreAleatoire = GenererNbAleatoire(1, 2);
		if (nombreAleatoire % 2 == 1)
		{
			nombreAleatoire = GenererNbAleatoire(0, 5);
			m_szPalindrome[m_iNbCaracteres / 2] = kszVoyelles[nombreAleatoire];
		}
		else
		{
			nombreAleatoire = GenererNbAleatoire(0, 20);
			m_szPalindrome[m_iNbCaracteres / 2] = kszConsonnes[nombreAleatoire];

		}
	}
	m_szPalindrome[m_iNbCaracteres] = '\0';
}
/******************************************************************************
	Ce constructeur copie permet de copier le param�tre re�us en param�tre
	dans le palindrome d'origine.
******************************************************************************/
CPalindrome::CPalindrome(const CPalindrome & copie)
{
	*this = copie;
}

/******************************************************************************
	Le destructeur permet de lib�rer l'espace m�moire allou� sur le tas et de 
	r�nitialiser les attributs � z�ro
******************************************************************************/
CPalindrome::~CPalindrome()
{
	if (m_szPalindrome != 0)
	{
		delete [] m_szPalindrome;
		m_szPalindrome = 0;
		m_iNbCaracteres = 0;
	}
}

bool CPalindrome::operator ==(const CPalindrome & Palin2) const
{
	if (this->m_iNbCaracteres != Palin2.m_iNbCaracteres)
	{
		return false;
	}

	bool identique = true;
	for (int i = 0; i < m_iNbCaracteres/2; i++)
	{
		if (m_szPalindrome[i] != Palin2.m_szPalindrome[i])
		{
			identique = false;
		}
	}
	return identique;
}
bool CPalindrome::operator !=(const CPalindrome & Palin2) const
{
    return !(*this == Palin2);
}
bool CPalindrome::operator <=(const CPalindrome & Palin2) const
{
	bool PlusGrandOuEgual = false;
	int nbDeCaractere;

	if (Palin2.m_iNbCaracteres < this->m_iNbCaracteres)
	{
		nbDeCaractere = Palin2.m_iNbCaracteres;
	}
	else
	{
		nbDeCaractere = this->m_iNbCaracteres;
	}

	int i = 0;
	do
	{
		if (this->m_szPalindrome[i] < Palin2.m_szPalindrome[i])
		{
			PlusGrandOuEgual = true;
		}
		if (this->m_szPalindrome[i] > Palin2.m_szPalindrome[i])
		{
			PlusGrandOuEgual = false;
		}

		i++;
	} while (this->m_szPalindrome[i] == Palin2.m_szPalindrome[i] && i <= nbDeCaractere / 2);

	if (i > nbDeCaractere / 2)
	{
		PlusGrandOuEgual = true;
	}

	return PlusGrandOuEgual;
}
bool CPalindrome::operator >=(const CPalindrome & Palin2) const
{
	bool PlusPetitOuEgual = false;
	int nbDeCaractere;

	if (Palin2.m_iNbCaracteres < this->m_iNbCaracteres)
	{
		nbDeCaractere = Palin2.m_iNbCaracteres;
	}
	else
	{
		nbDeCaractere = this->m_iNbCaracteres;
	}

	int i = 0;
	do
	{
		if (this->m_szPalindrome[i] > Palin2.m_szPalindrome[i])
		{
			PlusPetitOuEgual = true;
		}
		if (this->m_szPalindrome[i] < Palin2.m_szPalindrome[i])
		{
			PlusPetitOuEgual = false;
		}

		i++;
	} while (this->m_szPalindrome[i] == Palin2.m_szPalindrome[i] && i <= nbDeCaractere / 2);
	if (i > nbDeCaractere / 2)
	{
		PlusPetitOuEgual = true;
	}

	return PlusPetitOuEgual;
}
bool CPalindrome::operator <(const CPalindrome & Palin2) const
{
	bool bPlusPetit = false;
	int nbDeCaractere;

	if (Palin2.m_iNbCaracteres < this->m_iNbCaracteres)
	{
		nbDeCaractere = Palin2.m_iNbCaracteres;
	}
	else
	{
		nbDeCaractere = this->m_iNbCaracteres;
	}

	int i = 0;
	
	do {
		if (this->m_szPalindrome[i] < Palin2.m_szPalindrome[i])
		{
			bPlusPetit = true;
		}

		i++;
	} while (this->m_szPalindrome[i] == Palin2.m_szPalindrome[i] && i < nbDeCaractere);

	return bPlusPetit;
}
bool CPalindrome::operator >(const CPalindrome & Palin2) const
{
	bool bPlusGrand = false;
	int nbDeCaractere;

	if (Palin2.m_iNbCaracteres < this->m_iNbCaracteres)
	{
		nbDeCaractere = Palin2.m_iNbCaracteres;
	}
	else
	{
		nbDeCaractere = this->m_iNbCaracteres;
	}

	int i = 0;

	do {
		if (this->m_szPalindrome[i] > Palin2.m_szPalindrome[i])
		{
			bPlusGrand = true;
		}

		i++;
	} while (this->m_szPalindrome[i] == Palin2.m_szPalindrome[i] && i < nbDeCaractere);

	return bPlusGrand;
} 
const CPalindrome & CPalindrome::operator =(const CPalindrome & Palin2)
{
	if (m_szPalindrome != 0)
	{
		delete[] m_szPalindrome;
		m_szPalindrome = 0;
	}
	m_iNbCaracteres = Palin2.m_iNbCaracteres;
	if (m_iNbCaracteres > 0)
	{
		m_szPalindrome = new char[m_iNbCaracteres+1];
		for (int i = 0; i < m_iNbCaracteres+1; i++)
		{
			m_szPalindrome[i] = Palin2.m_szPalindrome[i];
		}
	}
	return *this;
} 
char * CPalindrome::GetChaine() const
{
	return m_szPalindrome;
}
int CPalindrome::GetNbCaracteres() const
{
	return m_iNbCaracteres;
} 


