#ifndef PALINDROME_H
#define PALINDROME_H
using namespace std;
#include <string>
#include "aleatoire.h"

const char kszConsonnes[20] = { 'b','c','d','f','g','h','j','k','l','m','n','p','q','r','s','t','v','w','x','z' };

const char kszVoyelles[6] = { 'a', 'e', 'i', 'o', 'u', 'y'};

class CPalindrome
{
public:
	//Constructeur
	CPalindrome();
	CPalindrome(const CPalindrome & copie);
	~CPalindrome();

	//Methodes	
	char * GetChaine() const;
	int GetNbCaracteres() const;
	bool operator > (const CPalindrome & Palin2) const;
	bool operator < (const CPalindrome & Palin2) const;
	bool operator >= (const CPalindrome & Palin2) const;
	bool operator <= (const CPalindrome & Palin2) const;
	bool operator != (const CPalindrome & Palin2) const;
	bool operator == (const CPalindrome & Palin2) const;
	const CPalindrome & operator = (const CPalindrome & Palin2); 
private:
	//Attributs
	char * m_szPalindrome;
	int m_iNbCaracteres;
};
#endif
