#include "listepalindromes.h"
/******************************************************************************
	Fichier:	principal.cpp

	Auteur:		Mikhail Kamara et Matthieu Martineau

	But:		Faire le déroulement 
******************************************************************************/

//Constante à utilliser lors des appels de la fonction system (...).
//À Noter que la déclaration est conditionnelle à l'environnement.
//Lorsque dans la console de Windows, la commande "cls" est utilisée.
//Ailleurs (dans les environnnements *nix), c'est la commande "clear" qui l'est.
#ifdef _WIN32
#define EFFACER_ECRAN "cls"
#else
#define EFFACER_ECRAN "clear"
#endif

#include <iostream>

/******************************************************************************
	La méthode main () permet de faire
******************************************************************************/
void SupprimerPalindrome(CPalindrome * pUnPalindrome)
{
	delete pUnPalindrome;
	pUnPalindrome = 0;
}

int main()
{
	const int i_nbPalindromes = 500;
	char cEntree;
	bool continuer = false;
	CListePalindromes * LesPalindromes=0;
	CListePalindromes * LesDoublonsPalindromes =0;
	unsigned int i_NbRejet = 0;

	do
	{
		system(EFFACER_ECRAN);
		cout << "Menu Principal" << endl;
		cout << "[1] Ajouts dans les listes\n";
		cout << "[2] Affichage des resultats\n";
		cout << "[3] Remises a zero\n";
		cout << "[4] Auteur\n";
		cout << "[5] Quitter\n";
		cout << "\nVotre choix: ";
		cEntree = getchar();

		while (getchar() != '\n');


		switch (cEntree)
		{
			case '1':
				system(EFFACER_ECRAN);
			{   cout << "Ajouts dans les listes"<< endl << endl;
				cout << "La liste principale existe-t-elle? ";
				
				
				if (LesPalindromes == 0)
				{
				    LesPalindromes = new CListePalindromes(SupprimerPalindrome);
					LesDoublonsPalindromes = new CListePalindromes(SupprimerPalindrome);
				}
				if (  LesPalindromes->EstVide())
				{
					bool bAjouterDansListe;
					cout << "Oui !"<<endl << endl;
					for (int i = 0; i < i_nbPalindromes; i++)
					{
						CPalindrome * unPalindrome = new CPalindrome();
						CPalindrome *uneCopiePalindrome = new CPalindrome(*unPalindrome);
						bAjouterDansListe = LesPalindromes->Inserer(unPalindrome);
						if (!bAjouterDansListe)
						{
							bAjouterDansListe = LesDoublonsPalindromes->Inserer(uneCopiePalindrome);
							if (!bAjouterDansListe)
							{
								i_NbRejet += 1;
							}
						}
						else
							delete uneCopiePalindrome;
					}

					cout << "Ajout de 500 palindromes a la liste principale" <<
						"(les doublons seront places dans la liste secondaire"
						"et les doublons des doublons serontrejetes)...";
				}
				else
				{
					cout << "Non !"<<endl << endl;
					cout << "Creation de la liste principale et de la liste de doublons... Ok!!" << endl << endl;
				}
				cout << "Ajout de 500 palindromes a la liste principale (les doublons seront places dans la liste secondaire et les doublons des doublons seront rejetes)..." << endl;
				cout << "Operation terminee!"<< endl<< endl;
			
				cout << "Appuyez sur <Entree> pour retourner au menu principal...";
				while (getchar() != '\n');
				break;
			}
		
			case '2':
			{
				system(EFFACER_ECRAN);
				cout << "Affichage des resultats" << endl <<endl;
				if (!LesPalindromes->EstVide())
				{
					cout << "La liste principale contient " << LesPalindromes->NbElements() << " palindromes." << endl << endl; 
					cout << "La liste secondaire contient" << LesDoublonsPalindromes->NbElements() <<" palindromes." << endl << endl; //mamqueuy
					cout << i_NbRejet << " palindromes ont ete rejetes." << endl << endl;
					cout << "Voici 7 palindromes choisis au hasard dans la liste principale:" << endl << endl;

					//BOUCLE
					string strPalindromeNum = "Palindrome no. ";
					int rnd;
					for (int i = 0; i < 7; i++)
					{
						rnd = GenererNbAleatoire(0, LesPalindromes->NbElements() - 1);
						printf("%i) %-4s %3i: %s \n", (i + 1), 
							strPalindromeNum.c_str(), rnd,(*LesPalindromes)[rnd].GetChaine());
					}
				}
				else
				{
					cout << "Les listes doivent avoir ete creees pour pouvoir voir les resultats." << endl << endl;
				}
				cout << "Appuyez sur <Entree> pour retourner au menu principal...";
					
				while (getchar() != '\n');
				break;
			}
			
			case '3':
			{
				system(EFFACER_ECRAN);
				cout << "Remises a zero" << endl << endl;
				if (!LesPalindromes->EstVide())
				{
					int i_nbElements = LesPalindromes->NbElements();
					int i_compteur = 0;
					for (int i = i_nbElements -1; i >= 0; i--)
					{
						LesPalindromes->Retirer(i);
					}
					cout << "Retrait des elements (du dernier au premier) de la"
						" liste principale... (" <<  i_nbElements <<
						" elements retires avec succes)" << endl << endl;
					i_nbElements = LesDoublonsPalindromes->NbElements();
					while (!LesDoublonsPalindromes->EstVide())
					{
						LesDoublonsPalindromes->Retirer(0);
						i_compteur++;
					}
					
					cout << "Retrait des elements (du premier au dernier) de la" 
						<< " liste secondaire... (" << i_compteur <<
						" elements retires avec succes)" << endl << endl;
					cout << "N.B.: Le nombre d'elements rejetes a ete remis"<< 
						" a zero." << endl << endl;				
				}
				else
				{
					cout << "Les listes doivent avoir ete creees pour pouvoir faire les remises a zero." << endl << endl;
				}
				cout << "Appuyez sur <Entree> pour retourner au menu principal...";

				while (getchar() != '\n');
				break;
			}
			case '4':
			{
				system(EFFACER_ECRAN);
				cout << "Travail 2 realise par Matthieu Martineau et"
					<< " Mikhail Kamara \n";
				cout << "Appuyez sur <Entree> pour retourner au menu principal... \n";

				while (getchar() != '\n');
				break;
			}
			case '5':
			{
				system(EFFACER_ECRAN);
				cout << "Operations de fin de programme" << endl << endl;
				cout << "La liste principale existe-t-elle?" << endl << endl;
				if (LesPalindromes !=0)
				{
					cout << " Oui!" << endl << endl; 
					CListePalindromes * LesPalindromesCopies = LesPalindromes;
					cout << "Construction d'une copie de la liste principale... Ok!" << endl << endl;

					cout << "La liste copie contient-elle des elements? " ;
					if (!LesPalindromesCopies->EstVide())
					{
						cout << "oui" << endl << endl;
						cout << "Premier element de la liste principale " <<
							"et de la liste copie: " << 
							(*LesPalindromes)[0].GetChaine() << 
							"<=>" << (*LesPalindromesCopies)[0].GetChaine() 
							<< endl << endl;
						cout << "Dernier element de la liste principale et de la liste copie: zyz <=> zyz" << endl << endl;
						cout << "Dernier element de la liste principale " <<
							"et de la liste copie: " <<
							(*LesPalindromes)
							[LesPalindromes->NbElements()-1].GetChaine() <<
							"<=>" << 
							(*LesPalindromesCopies)
							[LesPalindromesCopies->NbElements() -1].GetChaine()
							<< endl << endl;
					}
					else
					{
						cout << " Non!" << endl << endl;
					}
					LesPalindromes->Vider();
					LesDoublonsPalindromes->Vider();
					LesPalindromesCopies->Vider();
					cout << "Liberation de la liste principale, "
						<<"de la liste secondaire et de la liste copie... Ok!" 
						<< endl << endl;
				}
				else
				{
					cout << "Non!" << endl << endl; 
					cout << "Il n'y a donc rien a faire." << endl << endl;
				}
				cout << "Appuyez sur <Entree> pour quitter...";


				while (getchar() != '\n');
				break;
			}
			default:
			{
				cout << "Choix invalide. Appuyez sur <Entree> pour continuer...";
				while (getchar() != '\n');
				break;
			}
			
		}
		//while (getchar() != '\n');
	} while (cEntree != '5');

	return 0;
}
